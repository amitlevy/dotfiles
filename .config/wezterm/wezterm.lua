local w = require("wezterm")
local a = w.action

-- if you are *NOT* lazy-loading smart-splits.nvim (recommended)
local function is_vim(pane)
	-- this is set by the plugin, and unset on ExitPre in Neovim
	return pane:get_user_vars().IS_NVIM == "true"
end

local direction_keys = {
	Left = "h",
	Down = "j",
	Up = "k",
	Right = "l",
	-- reverse lookup
	h = "Left",
	j = "Down",
	k = "Up",
	l = "Right",
}

local function split_nav(resize_or_move, key)
	return {
		key = key,
		mods = resize_or_move == "resize" and "META" or "CTRL",
		action = w.action_callback(function(win, pane)
			if is_vim(pane) then
				-- pass the keys through to vim/nvim
				win:perform_action({
					SendKey = { key = key, mods = resize_or_move == "resize" and "META" or "CTRL" },
				}, pane)
			else
				if resize_or_move == "resize" then
					win:perform_action({ AdjustPaneSize = { direction_keys[key], 3 } }, pane)
				else
					win:perform_action({ ActivatePaneDirection = direction_keys[key] }, pane)
				end
			end
		end),
	}
end

local config = {
	color_scheme = "Catppuccin Mocha",
	font_size = 14.0,
	-- font = w.font("MesloLGL Nerd Font"),
	default_prog = { "nu" },
	use_fancy_tab_bar = false,
	pane_focus_follows_mouse = true,
	hide_tab_bar_if_only_one_tab = true,
	window_background_opacity = 0.7,
	window_decorations = "NONE",
	window_padding = {
		left = 0,
		right = 0,
		top = 0,
		bottom = 0,
	},
	inactive_pane_hsb = {
		saturation = 0.8,
		brightness = 0.7,
	},
	keys = {
		-- move between split panes
		split_nav("move", "h"),
		split_nav("move", "j"),
		split_nav("move", "k"),
		split_nav("move", "l"),
		-- resize panes
		split_nav("resize", "h"),
		split_nav("resize", "j"),
		split_nav("resize", "k"),
		split_nav("resize", "l"),
		{
			key = "|",
			mods = "ALT",
			action = a.SplitHorizontal({ domain = "CurrentPaneDomain" }),
		},
		{
			key = "\\",
			mods = "ALT",
			action = a.SplitHorizontal({ domain = "CurrentPaneDomain" }),
		},
		{
			key = "p",
			mods = "ALT",
			action = a.SplitHorizontal({ domain = "CurrentPaneDomain" }),
		},
		{
			key = "d",
			mods = "ALT",
			action = a.SplitVertical({ domain = "CurrentPaneDomain" }),
		},
		{
			key = "c",
			mods = "ALT",
			action = a.CopyTo("ClipboardAndPrimarySelection"),
		},
		{
			key = "v",
			mods = "ALT",
			action = a.PasteFrom("Clipboard"),
		},
		{
			key = "a",
			mods = "ALT",
			action = a.ToggleFullScreen,
		},
		{
			key = "m",
			mods = "CMD",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "t",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "t",
			mods = "ALT",
			action = a.SpawnTab("CurrentPaneDomain"),
		},
		{
			key = "w",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "w",
			mods = "ALT",
			action = a.CloseCurrentTab({ confirm = true }),
		},
		{
			key = "r",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "r",
			mods = "ALT",
			action = a.ReloadConfiguration,
		},
		{
			key = "f",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "f",
			mods = "ALT",
			action = a.Search({ CaseSensitiveString = "" }),
		},
		{
			key = "r",
			mods = "CTRL|SHIFT",
			action = a.ActivateKeyTable({
				name = "resize_pane",
				one_shot = false,
			}),
		},
		{
			key = "1",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "1",
			mods = "ALT",
			action = a.ActivateTab(0),
		},
		{
			key = "2",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "2",
			mods = "ALT",
			action = a.ActivateTab(1),
		},
		{
			key = "3",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "3",
			mods = "ALT",
			action = a.ActivateTab(2),
		},
		{
			key = "4",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "4",
			mods = "ALT",
			action = a.ActivateTab(3),
		},
		{
			key = "5",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "5",
			mods = "ALT",
			action = a.ActivateTab(4),
		},
		{
			key = "6",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "6",
			mods = "ALT",
			action = a.ActivateTab(5),
		},
		{
			key = "7",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "7",
			mods = "ALT",
			action = a.ActivateTab(6),
		},
		{
			key = "8",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "8",
			mods = "ALT",
			action = a.ActivateTab(7),
		},
		{
			key = "9",
			mods = "SUPER",
			action = a.DisableDefaultAssignment,
		},
		{
			key = "9",
			mods = "ALT",
			action = a.ActivateTab(-1),
		},
		{
			key = "E",
			mods = "CTRL|SHIFT",
			action = a.PromptInputLine({
				description = "Enter new name for tab",
				action = w.action_callback(function(window, pane, line)
					-- line will be `nil` if they hit escape without entering anything
					-- An empty string if they just hit enter
					-- Or the actual line of text they wrote
					if line then
						window:active_tab():set_title(line)
					end
				end),
			}),
		},
	},
	key_tables = {
		resize_pane = {
			{ key = "LeftArrow", action = a.AdjustPaneSize({ "Left", 1 }) },
			{ key = "h", action = a.AdjustPaneSize({ "Left", 1 }) },

			{ key = "RightArrow", action = a.AdjustPaneSize({ "Right", 1 }) },
			{ key = "l", action = a.AdjustPaneSize({ "Right", 1 }) },

			{ key = "UpArrow", action = a.AdjustPaneSize({ "Up", 1 }) },
			{ key = "k", action = a.AdjustPaneSize({ "Up", 1 }) },

			{ key = "DownArrow", action = a.AdjustPaneSize({ "Down", 1 }) },
			{ key = "j", action = a.AdjustPaneSize({ "Down", 1 }) },

			-- Cancel the mode by pressing escape
			{ key = "Escape", action = "PopKeyTable" },
		},
	},
}

return config
