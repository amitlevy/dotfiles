return {
  {
    "ThePrimeagen/refactoring.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-treesitter/nvim-treesitter",
    },
    keys = {
      {
        "<leader>rf",
        "<cmd>Refactor extract <cr>",
        mode = "x",
        desc = "Extract Function",
      },
      {
        "<leader>rF",
        "<cmd>Refactor extract_to_file <cr>",
        mode = "x",
        desc = "Extract Function To File",
      },
      {
        "<leader>rv",
        "<cmd>Refactor extract_var <cr>",
        mode = "x",
        desc = "Extract Variable",
      },
      {
        "<leader>rb",
        "<cmd>Refactor extract_block<cr>",
        mode = "n",
        desc = "Extract Block",
      },
      {
        "<leader>rB",
        "<cmd>Refactor extract_block_to_file<cr>",
        mode = "n",
        desc = "Extract Block To File",
      },
    },
  },
  {
    "folke/which-key.nvim",
    opts = {
      spec = {
        { "<leader>r", group = "refactor" },
      },
    },
  },
}
