return {
  "nvim-treesitter/nvim-treesitter",
  dependencies = {
    -- Install official queries and filetype detection
    -- alternatively, see section "Install official queries only"
    { "nushell/tree-sitter-nu" },
  },
  build = ":TSUpdate",
}
